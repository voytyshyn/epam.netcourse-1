using System;

namespace Task3
{
	public class ArrayCust
	{
		public static void Main()
		{
			//Створення об'єктів
            var vector1 = new Vector(-4, 7);
            var vector2 = new Vector(-4, 7);
            Vector res;
            
			//Заповнення векторів випадковими значеннями
            Vector.FillRnd(vector1);
            Vector.FillRnd(vector2);
            
            Console.WriteLine("Vector1: [ {0}]", vector1);
            Console.WriteLine("Vector2: [ {0}]", vector2);
            
            // Додавання двох векторів.
            res = vector1 + vector2;
            Console.WriteLine("Addition of two vectors: [ {0}]", res);

            // Віднімання двох веткорів.
            res = vector1 - vector2;
            Console.WriteLine("Substraction of two vectors: [ {0}]", res);
            
            // Множення вектора на скаляр.
            res = vector1 * 2;
            Console.WriteLine("Multiplication by value: [ {0}]", res);

            // Порівняння двох векторів.
            Console.WriteLine("vector1 < vector2 ?: {0}", vector1 < vector2);
		}
	}
	
	public class Vector
	{
		#region Field
		private Array _array;
		private static Random number = new Random();
		#endregion
		
		#region Constructor
		public Vector(int count)
		{
			_array = Array.CreateInstance(typeof(int), new int[] { count }, new int[] { 0 });
		}
		
		public Vector(int lLimit, int uLimit)
        {
            _array = Array.CreateInstance(typeof(int), new int[] { uLimit - lLimit + 1 }, new int[] { lLimit });
        }
		#endregion
		
		#region Properties
        //Length of array.
        public int Length
        {
            get
            {
                return _array.Length;
            }
        }
		
		// Lower bound of array.
        public int LowerBound
        {
            get
            {
                return _array.GetLowerBound(0);
            }
        }

        // Upper bound of array.
        public int UpperBound
        {
            get
            {
                return _array.GetUpperBound(0);
            }
        }
		#endregion
		
		#region Indexer
        public int this[int index]
        {
            get
            {
               return (int)_array.GetValue(index);
            }

            set
            {
                _array.SetValue(value, index);
            }
        }
        #endregion
		
		#region Methods
		public static void FillRnd(Vector vector)
        {
            for (var i = vector.LowerBound; i <= vector.UpperBound; i++)
            {
                vector[i] = number.Next(-47,47);
            }
        }
        // Add two vectors
        public static Vector Add(Vector vector1, Vector vector2)
        {
            var buf =  new Vector(vector1.LowerBound, vector2.UpperBound);
            
			for (var i = vector1.LowerBound; i <= vector1.UpperBound; i++)
            {
                buf[i] = vector1[i] + vector2[i];
            }

            return buf;
        }

        // Substract two vectors
        public static Vector Substract(Vector vector1, Vector vector2)
        {
            var buf = new Vector(vector1.LowerBound, vector2.UpperBound);
            
			for (var i = vector1.LowerBound; i <= vector1.UpperBound; i++)
            {
                buf[i] = vector1[i] - vector2[i];
            }

            return buf;
        }

        // Multiplication vector * scalar.
        public static Vector Multiply(Vector vector, int value)
        {
            var buf = new Vector(vector.LowerBound, vector.UpperBound);
            for (var i = vector.LowerBound; i <= vector.UpperBound; i++)
            {
                buf[i] = vector[i] * value;
            }

            return buf;
        }

        public override string ToString()
        {
            var result = "";
            
            foreach(int number in _array)
            {
				result += string.Format("{0} ", number);        
            }
            return result;   
        }
        #endregion
		
		#region Operators
        //Compare vectors by length
        public static bool operator <(Vector vector1, Vector vector2)
		{
			return (vector1.Length < vector2.Length);
		}

		public static bool operator >(Vector vector1, Vector vector2)
		{
			return (vector1.Length > vector2.Length);
		}
        
        public static Vector operator +(Vector vector1, Vector vector2)
        {
            return Vector.Add(vector1, vector2);
        }

        public static Vector operator -(Vector vector1, Vector vector2)
        {
            return Vector.Substract(vector1, vector2);
        }

        public static Vector operator *(Vector vector1, int number)
        {
            return Vector.Multiply(vector1, number);
        }
        #endregion
	}
}